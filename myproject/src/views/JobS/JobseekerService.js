const jobseekerService = {
  jobseekerList: [
    { id: 1, fname: 'Taneat', lname: 'Taddsab', idcrad: '1258866477859', gender: 'M', age: 20, address: 'chonburi', tel: '0999999999' },
    { id: 2, fname: 'Saharud', lname: 'Seerakoon', idcrad: '1278899654258', gender: 'M', age: 20, address: 'chonburi', tel: '0911111111' },
    { id: 3, fname: 'Siriphonpha', lname: 'Fangnakham', idcrad: '1276633211458', gender: 'F', age: 20, address: 'chonburi', tel: '0987654321' }
  ],
  listId: 3,
  addJobseeker (jobseeker) {
    jobseeker.id = this.listId++
    this.jobseekerList.push(jobseeker)
  },
  updateJobseeker (jobseeker) {
    const index = this.jobseekerList.findIndex(item => item.id === jobseeker.id)
    this.jobseekerList.splice(index, 1, jobseeker)
  },
  deleteJobseeker (jobseeker) {
    const index = this.jobseekerList.findIndex(item => item.id === jobseeker.id)
    this.jobseekerList.splice(index, 1)
  },
  getJobseekers () {
    return [...this.jobseekerList]
  }
}
export default jobseekerService
